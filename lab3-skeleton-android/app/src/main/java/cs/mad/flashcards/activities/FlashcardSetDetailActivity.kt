package cs.mad.flashcards.activities

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class FlashcardSetDetailActivity : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    private var addButton: Button? = null
    private var sets: ArrayList<Flashcard> = Flashcard.getHardcodedFlashcards() as ArrayList<Flashcard>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcardsetdetail)

        recyclerView = findViewById(R.id.recycler_2)
        var adapter = FlashcardAdapter(sets)
        recyclerView?.adapter = adapter

        addButton = findViewById<Button>(R.id.button1)
        addButton?.setOnClickListener{
            sets.add(Flashcard("New Term", "New Definition"))
            adapter.notifyItemInserted(sets.size-1)
        }
    }
}