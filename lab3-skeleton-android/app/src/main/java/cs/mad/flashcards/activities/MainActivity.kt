package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet

/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    private var addButton: Button? = null
    private var sets: ArrayList<FlashcardSet> = FlashcardSet.getHardcodedFlashcardSets() as ArrayList<FlashcardSet>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recycler_1)
        var adapter = FlashcardSetAdapter(sets)
        recyclerView?.adapter = adapter

        addButton = findViewById<Button>(R.id.button4)
        addButton?.setOnClickListener{
            sets.add(FlashcardSet("New Title"))
            adapter.notifyItemInserted(sets.size-1)
        }

                /*
            connect to views using findViewById
            setup views here - recyclerview, button
            don't forget to notify the adapter if the data set is changed
         */
    }
}